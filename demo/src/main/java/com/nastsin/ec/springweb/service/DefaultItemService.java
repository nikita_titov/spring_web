package com.nastsin.ec.springweb.service;

import com.nastsin.ec.springweb.entity.Item;
import com.nastsin.ec.springweb.exception.NotFoundException;
import com.nastsin.ec.springweb.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DefaultItemService implements ItemService {

    private final ItemRepository itemRepository;

    @Autowired
    public DefaultItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public Item getItem(Long id) throws NotFoundException {
        Optional<Item> item = itemRepository.findById(id);
        if (item.isPresent()) {
            return item.get();
        } else {
            throw new NotFoundException("Item with id: " + id + " was not found");
        }
    }

    @Override
    public Item saveItem(Item item) throws NotFoundException{
        try {
            return this.itemRepository.save(item);
        }catch (JpaObjectRetrievalFailureException exception){
            throw new NotFoundException("User with id: " + item.getUser().getId() + " was not found");
        }
    }

    @Override
    public List<Item> getItems() {
        return null;
    }

    @Override
    public void deleteItem(Long id){
        itemRepository.deleteById(id);
    }
}
